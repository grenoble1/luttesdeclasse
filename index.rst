.. sidebar:: Egalité économique et sociale, Anarcho-eco-socialisme !

    :Version: |version|

.. _luttes_de_classe_grenoble:

=================================================================
**Luttes de classe Grenoble**
=================================================================

.. seealso::

   - https://38.demosphere.net/
   - https://paris.demosphere.net/
   - https://greve.cool/
   - :ref:`genindex`
   - https://framagit.org/grenoble1/luttesdeclasse


.. figure:: contre_les_tyrans.png
   :align: center

   https://www.hacking-social.com/2019/12/03/comment-desobeir-quelques-listes/

.. toctree::
   :maxdepth: 6

   assr38/assr38
   combattreantisemitisme/combattreantisemitisme
   nonaufascisme/nonaufascisme
   nonalaretraiteapoints/nonalaretraiteapoints
   urgenceclimatique/urgenceclimatique

.. toctree::
   :maxdepth: 7

   nouvelles/nouvelles
   tracts/tracts
   index/index
   meta/meta
