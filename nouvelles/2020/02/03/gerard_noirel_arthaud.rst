.. index::
   pair: Gérard Noirel ; Arthaud
   pair: Livre ; Une histoire populaire de la France

.. _gerard_noirel_arthaud_2020_02_03:

===========================================================
**Une histoire populaire de la France** par Gérard Noirel
===========================================================

.. seealso::

   - http://www.librairie-arthaud.fr/events.php?blid=5676


.. figure:: gerard_noirel/gerard_noiriel_arthaud.png
   :align: center

   http://www.librairie-arthaud.fr/events.php?blid=5676


Présentation
================

Lundi 3 février 2020 à 18h, rencontre avec Gérard Noirel autour de son ouvrage
**Une histoire populaire de la France**

Rencontre en partenariat avec l'association ERE-éducation république égalité

Dans cette somme, l’auteur a voulu éclairer la place et le rôle du peuple dans
tous les grands événements et les grandes luttes qui ont scandé son histoire
depuis la fin du Moyen Âge : les guerres, l’affirmation de l’État, les révoltes
et les révolutions, les mutations économiques et les crises, l’esclavage et la
colonisation, les migrations, les questions sociale et nationale.

La France, c’est ici l’ensemble des territoires (colonies comprises) qui ont
été placés, à un moment ou un autre, sous la coupe de l’État français.

Historien, Gérard Noiriel a notamment travaillé sur l’articulation de
l’immigration, de la nation et des sentiments xénophobes.

Il a publié en 2018 Une Histoire populaire de la France, synthèse de toute une
vie de recherches et d’engagements.

(Attention salle de conférence au 3ème étage sans accès ascenseur)
