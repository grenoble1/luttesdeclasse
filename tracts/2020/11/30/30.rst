.. index::
   ! Sécurité globale

.. _tracts_gre_2020_11_30:

===============================================================
Les députéEs qui ont voté le projet de loi sécurité globale
===============================================================


.. figure:: Isere-Secu-Globale_2.jpg
   :align: center


:download:`Isere-Secu-Globale.pdf`
