.. index::
   ! meta-infos


.. _luttes_meta_infos:

=====================
Meta infos
=====================

.. seealso::

   - https://cntvignoles.frama.io/meta/

.. contents::
   :depth: 3


Gitlab project
================

.. seealso::

   - https://framagit.org/grenoble1/luttesdeclasse


Issues
--------

.. seealso::

   - https://framagit.org/grenoble1/luttesdeclasse/-/boards


root directory
===============

::

    $ ls -als

::


    $ ls -als
    total 252
    4 drwxrwxrwx 13 4096 nov.  30 10:06 .
    4 drwxr-xr-x  3 4096 mai    3  2020 ..
    4 drwxrwxrwx  2 4096 mai    3  2020 assr38
    4 drwxr-xr-x  4 4096 mai    3  2020 _build
    4 drwxrwxrwx  2 4096 mai    3  2020 combattreantisemitisme
    4 -rwxrwxrwx  1 3682 nov.  30 10:06 conf.py
    76 -rw-rw-rw-  1 pvergain pvergain 76971 mai    3  2020 contre_les_tyrans.png
    4 -rw-r--r--  1   93 oct.  27 18:29 feed.xml
    4 drwxrwxrwx  8 4096 nov.  30 10:03 .git
    4 -rwxrwxrwx  1   49 mai    3  2020 .gitignore
    4 -rw-rw-rw-  1  211 oct.  26 19:58 .gitlab-ci.yml
    4 drwxr-xr-x  2 4096 oct.  19 18:09 index
    4 -rwxrwxrwx  1  842 mai    3  2020 index.rst
    4 -rw-rw-rw-  1 1154 mars  30  2020 Makefile
    4 drwxr-xr-x  2 4096 nov.  30 10:06 meta
    4 drwxrwxrwx  2 4096 mai    3  2020 nonalaretraiteapoints
    4 drwxrwxrwx  2 4096 mai    3  2020 nonaufascisme
    4 drwxrwxrwx  3 4096 nov.  30 09:57 nouvelles
    84 -rw-r--r--  1 pvergain pvergain 84744 nov.  30 10:02 poetry.lock
    4 -rw-rw-rw-  1 1017 oct.  26 20:03 .pre-commit-config.yaml
    4 -rw-rw-rw-  1  307 oct.  26 20:01 pyproject.toml
    4 -rw-rw-rw-  1   18 mai    3  2020 README.md
    4 -rw-rw-rw-  1 2060 nov.  30 10:02 requirements.txt
    4 drwxr-xr-x  3 4096 nov.  30 09:58 tracts
    4 drwxrwxrwx  2 4096 mai    3  2020 urgenceclimatique


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
